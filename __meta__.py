DATA = {
    # Ordered by commit log entries
    "author": ('mikefromit <mike.arbelaez@gmail.com>'),
    "author_email": 'pythondev.slack@gmail.com',
    "copyright": 'Copyright 2016 Python Developers Community',
    "description": 'The pythondev slack community reaction website',
    "license": 'Apache 2.0',
    "name": 'pythondev-reactions',
    "url": 'https://gitlab.com/PythonDevCommunity/pythondev-reactions',
    # Versions should comply with PEP440. For a discussion on
    # single-sourcing the version across setup.py and the project code,
    # see http://packaging.python.org/en/latest/tutorial.html#version
    "version": '0.0.1',
}
