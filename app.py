from flask import Flask
from flask import redirect
from flask import render_template
from flask import url_for

app = Flask(__name__)

reactions = [
    {
        'id': 1,
        'text': 'When they tell me I\'ll be taking over the legacy PHP project.',
        'img_url': 'https://67.media.tumblr.com/c2910f5eaf259d33a8f3f5d1f1f70e42'
                   '/tumblr_odjoplUm701s373hwo1_250.gif',
        'username': 'skift',
        'likes': 1,
        'comments': [
            {'id': 1,
             'username': 'mikefromit',
             'text': 'hahahaha'
             }
        ]
    },
{
        'id': 2,
        'text': 'When my PR was merged into master.',
        'img_url': 'https://67.media.tumblr.com/9e5cbc54bc8ebf2f5e49900861a81426'
                   '/tumblr_odqr13OKYS1s373hwo1_250.gif',
        'username': 'ovv',
        'likes': 1,
        'comments': [
            {'id': 1,
             'username': 'mrasband',
             'text': 'awesome!'
             }
        ]
    }
]


@app.route('/favicon.ico')
def favicon():
    return redirect(url_for('static', filename='img/favicon.ico'))


@app.route('/')
def index():
    return render_template('index.html', reactions=reactions)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True, use_reloader=True)
